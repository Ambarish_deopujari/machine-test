<?php
class Calculator 
{
    /*Method to multiply values in array
     * @params(array)
     * @returns(sum of elements of array)
     */
    function multiply($values) 
    {
        $sum = 1;
        foreach($values as $key=>$value) {
            if($value < 1000)
                $sum*= $value;
        }
        return $sum;
    }
    
    /*Method to check whether elements of array are numeric
     * @params(array)
     * @returns(true/false)
     */
    function is_numeric($array)
    {
        $negative_nos = [];
        foreach($array as $value) {
            if(!is_numeric($value) && $value != '') {
                return false;
            }
            if(intval($value)<0)
                array_push($negative_nos,intval($value));
        }
        if(count($negative_nos)>0){
            throw new Exception("Negative numbers(". implode(",", $negative_nos) .")not allowed");
        }

        return true;
    }
    
}

if(isset($argv[1])) {
    switch($argv[1]) {
        case "multiply":
            $values = isset($argv[2]) ? $argv[2] : '';

            $calculator = new Calculator();
            try {
                if ($values == '') {
                    echo "0";
                } else if($calculator->is_numeric(explode(',', $values))){
                    echo $calculator->multiply(explode(',', $values));
                } else {
                    echo "Please specify numeric values";
                }
            } catch(Exception $e) {
                echo 'Error: ' .$e->getMessage();
            }
            break;

        default:
            echo "Unknown operation";
    }
} else echo "Please specify operation to be performed";
?>